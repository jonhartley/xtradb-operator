# xtradb-operator
Convert Helm to operator

## Deploy Operator

- kubectl -n YOURNAMESPACE -f create deploy/crds/xtradb.mysql_xtradbs_crd.yaml
- kubectl -n YOURNAMESPACE -f create deploy/rbac.yaml
- kubectl -n YOURNAMESPACE -f create deploy/operator.yaml

## Basic Cluster

- Secrets file needs to be created first
- The only ting that needs to be added to CRD is secretname: and size: 
- All the rest are defaults, to override simply uncomment and set appropriately 

~~~~
apiVersion: v1
kind: Secret
metadata:
  name: mysql-creds
type: Opaque
data:
  root: cGFzc3dvcmQ=
  sstuser: cGFzc3dvcmQ=
  replication: cmVwbGljYXRpb246cGFzc3dvcmQK
  monitor: cGFzc3dvcmQ=
  exporterdsn: bW9uaXRvcjpwYXNzd29yZEAoMTI3LjAuMC4xOjMzMDYpLw==
---
apiVersion: xtradb.mysql/v1alpha1
kind: XtraDB
metadata:
  name: mysql-pxc
spec:
  secretsname: "mysql-creds"
  size: 3
# Defaults are set for these
# cores: "500m"
# memory: "2000Mi"
# mainstoragesize: "10G"
# mainstorageclass: "standard"
# backupstorageclass: "nfs"
# backupstoragesize: "40G"
# extraconfig: "max_allowed_packet=1G,innodb_buffer_pool_size=1G,max_connections=500,innodb_log_file_size=128M"
# wsrepconfig: "wsrep_provider_options='gcache.size=512M;gcache.recover=yes;evs.send_window=512;evs.user_send_window=512'"
# binlogconfig: "expire_logs_days=1"
# pxcimage: "jonhartley/k8s-pxc:latest"
# exporterimage: "prom/mysqld-exporter:latest" 
# nodeselector: true
# nodelabel: "workload"
# nodevalue: "persistence" 
# tolerations: true
# tolkey: "workload"
# tolvalue: "persistence" 
~~~~

## Developing

### Main files are

- pkg/apis/xtradb/v1alpha/xtradb_types.go  -  types for input
- pkg/controller/xtradb/xtradb_controller.go  -  all kube objects, watchers and config live in here

### When changing files

- /xtradb_types.go  -  need ro run operator-sdk generate k8s to regenerate deepcopy funcs

### To Build

- operator-sdk build DOCKERTAG  -  e.g. operator-sdk build local/xtradb-operator:v1.0.0

## TO DO

- Add backup funtionallity
- Add replica capability
- Add external service for replication
- Add audit logging
- Add sql-mode override

