// Package v1alpha1 contains API Schema definitions for the xtradb v1alpha1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=xtradb.home.org
package v1alpha1
