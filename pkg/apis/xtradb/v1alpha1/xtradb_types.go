package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// XtraDBSpec defines the desired state of XtraDB
type XtraDBSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	SecretsName string `json:"secretsname"`
	Size int32 `json:"size"`
	Mainstoragesize string `json:"mainstoragesize"`
	Mainstorageclass string `json:"mainstorageclass"`
	BackupEnabled bool `json:"backupenabled"`
	Backupstoragesize string `json:"backupstoragesize"`
	Backuppvname string `json:"backuppvname"`
	Backupstorageclass string `json:"backupstorageclass,omitempty"`
	Cores string `json:"cores"`
	Memory string `json:"memory"`
	ExtraConfig string `json:"extraconfig"`
	WsrepConfig string `json:"wsrepoconfig"`
	BinlogConfig string `json:"binlogconfig"`
	ImagePXC string `json:"pxcimage"`
	ImageExporter string `json:"exporterimage"`
	ImageMaint string `json:"maintimage"`
	ImageFluentd string `json:"maintfluentd"`
	NodeSelector bool `json:"nodeselector"`
	NodeLabel string `json:"nodelabel"`
	NodeValue string `json:"nodevalue"`
	Tolerations bool `json:"tolerations"`
	TolKey string `json:"tolkey"`
	TolValue string `json:"tolvalue"`
}

// XtraDBStatus defines the observed state of XtraDB
type XtraDBStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	Nodes []string `json:"nodes"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// XtraDB is the Schema for the xtradbs API
// +k8s:openapi-gen=true
type XtraDB struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   XtraDBSpec   `json:"spec,omitempty"`
	Status XtraDBStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// XtraDBList contains a list of XtraDB
type XtraDBList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []XtraDB `json:"items"`
}

func init() {
	SchemeBuilder.Register(&XtraDB{}, &XtraDBList{})
}
