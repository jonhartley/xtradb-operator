package xtradb

import (
	"context"
	cmp "github.com/google/go-cmp/cmp"
	//"fmt"
	//"time"
	//"strings"
	"strconv"

	xtradbv1alpha1 "home.org/xtradb-operator/pkg/apis/xtradb/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
    batchv1beta1 "k8s.io/api/batch/v1beta1"
    jobv1 "k8s.io/api/batch/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	policyv1beta1 "k8s.io/api/policy/v1beta1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"k8s.io/apimachinery/pkg/api/resource"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_xtradb")
var Restarts = int(0)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new XtraDB Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileXtraDB{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("xtradb-controller", mgr, controller.Options{Reconciler: r, MaxConcurrentReconciles: 6})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource XtraDB
	err = c.Watch(&source.Kind{Type: &xtradbv1alpha1.XtraDB{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	// TODO(user): Modify this to be the types you create that are owned by the primary resource
	// Watch for changes to secondary resource Pods and requeue the owner XtraDB
	err = c.Watch(&source.Kind{Type: &corev1.Pod{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &corev1.Service{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &policyv1beta1.PodDisruptionBudget{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &corev1.PersistentVolumeClaim{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &corev1.ServiceAccount{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &rbacv1.Role{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &rbacv1.RoleBinding{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &batchv1beta1.CronJob{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &appsv1.StatefulSet{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &xtradbv1alpha1.XtraDB{},
	})
	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileXtraDB implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileXtraDB{}

// ReconcileXtraDB reconciles a XtraDB object
type ReconcileXtraDB struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a XtraDB object and makes changes based on the state read
// and what is in the XtraDB.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  This example creates
// a Pod as an example
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileXtraDB) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling XtraDB")

	// Fetch the XtraDB instance
	instance := &xtradbv1alpha1.XtraDB{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

    // create PVs in advance 
    founddatavol := &corev1.PersistentVolumeClaim{}
    datavol := &corev1.PersistentVolumeClaim{}
	for nodenum := 0; nodenum < 3; nodenum++ {
		datavol = DataDir(instance, int32(nodenum))
	
		if err := controllerutil.SetControllerReference(instance, datavol, r.scheme); err != nil {
			return reconcile.Result{}, err
		}
		err = r.client.Get(context.TODO(), types.NamespacedName{Name: datavol.Name, Namespace: datavol.Namespace}, founddatavol)
		if err != nil && errors.IsNotFound(err) {
			reqLogger.Info("Creating a new PersistentVolumeClaim", "PersistentVolumeClaim.Namespace", datavol.Namespace, "PersistentVolumeClaim.Name", datavol.Name)
			err = r.client.Create(context.TODO(), datavol)
			if err != nil {
				return reconcile.Result{}, err
			}
			return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		! cmp.Equal(founddatavol.Spec.Resources.Requests, datavol.Spec.Resources.Requests)) {
	reqLogger.Info("VolumeRequest has changed", "PersistentVolume.Namespace", datavol.Namespace, "PersistentVolume.Name", datavol.Name)
	founddatavol.Spec.Resources.Requests = datavol.Spec.Resources.Requests
	Restarts = Restarts + 1
	err = r.client.Update(context.TODO(), founddatavol)
		if err != nil {
			reqLogger.Error(err, "Failed to update PersistentVolume", "PersistentVolume.Namespace", datavol.Namespace, "PersistentVolume.Name", datavol.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
			return reconcile.Result{}, err
		}

	}

	// Service 1

	svc1 := Service1(instance)

	if err := controllerutil.SetControllerReference(instance, svc1, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundsvc1 := &corev1.Service{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: svc1.Name, Namespace: svc1.Namespace}, foundsvc1)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new Service", "Service.Namespace", svc1.Namespace, "Service.Name", svc1.Name)
		err = r.client.Create(context.TODO(), svc1)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		! cmp.Equal(foundsvc1.Spec.Selector, svc1.Spec.Selector)) {
	reqLogger.Info("Service1 has changed", "Service.Namespace", svc1.Namespace, "Service.Name", svc1.Name)
	foundsvc1.Spec.Selector = svc1.Spec.Selector
	err = r.client.Update(context.TODO(), foundsvc1)
		if err != nil {
			reqLogger.Error(err, "Failed to update Service", "Service.Namespace", svc1.Namespace, "Service.Name", svc1.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

    // end of service 1

    // Service 2

	svc2 := Service2(instance)

	if err := controllerutil.SetControllerReference(instance, svc2, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundsvc2 := &corev1.Service{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: svc2.Name, Namespace: svc2.Namespace}, foundsvc2)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new Service", "Service.Namespace", svc2.Namespace, "Service.Name", svc2.Name)
		err = r.client.Create(context.TODO(), svc2)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		! cmp.Equal(foundsvc2.Spec.Selector, svc2.Spec.Selector) ||		
		foundsvc2.Spec.SessionAffinity != svc2.Spec.SessionAffinity ) {
	reqLogger.Info("Service2 has changed", "Service.Namespace", svc2.Namespace, "Service.Name", svc2.Name)
	foundsvc2.Spec.Selector = svc2.Spec.Selector
	foundsvc2.Spec.SessionAffinity = svc2.Spec.SessionAffinity
	err = r.client.Update(context.TODO(), foundsvc2)
		if err != nil {
			reqLogger.Error(err, "Failed to update Service", "Service.Namespace", svc2.Namespace, "Service.Name", svc2.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

    // end of service 2

    // pdb

    pdb1 := Pdb1(instance)

	if err := controllerutil.SetControllerReference(instance, pdb1, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundpdb1 := &policyv1beta1.PodDisruptionBudget{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: pdb1.Name, Namespace: pdb1.Namespace}, foundpdb1)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new PodDisruptionBudget", "PodDisruptionBudget.Namespace", pdb1.Namespace, "PodDisruptionBudget.Name", pdb1.Name)
		err = r.client.Create(context.TODO(), pdb1)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		! cmp.Equal(foundpdb1.Spec.Selector, pdb1.Spec.Selector) ||
		*foundpdb1.Spec.MaxUnavailable != *pdb1.Spec.MaxUnavailable ) {
	reqLogger.Info("PodDisruptionBudget has changed", "PodDisruptionBudget.Namespace", pdb1.Namespace, "PodDisruptionBudget.Name", pdb1.Name)
	foundpdb1.Spec.MaxUnavailable = pdb1.Spec.MaxUnavailable
    foundpdb1.Spec.Selector = pdb1.Spec.Selector
	err = r.client.Update(context.TODO(), foundpdb1)
		if err != nil {
			reqLogger.Error(err, "Failed to update PodDisruptionBudget", "PodDisruptionBudget.Namespace", pdb1.Namespace, "PodDisruptionBudget.Name", pdb1.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

	// backup vol

	if instance.Spec.BackupEnabled == true {

    sa1 := SA1(instance)

	if err := controllerutil.SetControllerReference(instance, sa1, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundsa1 := &corev1.ServiceAccount{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: sa1.Name, Namespace: sa1.Namespace}, foundsa1)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new ServiceAccount", "ServiceAccount.Namespace", sa1.Namespace, "ServiceAccount.Name", sa1.Name)
		err = r.client.Create(context.TODO(), sa1)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		foundsa1.Name != sa1.Name ) {
	reqLogger.Info("ServiceAccount has changed", "ServiceAccount.Namespace", sa1.Namespace, "ServiceAccount.Name", sa1.Name)
	foundsa1.Name = sa1.Name
	err = r.client.Update(context.TODO(), foundsa1)
		if err != nil {
			reqLogger.Error(err, "Failed to update ServiceAccount", "ServiceAccount.Namespace", sa1.Namespace, "ServiceAccount.Name", sa1.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

    role1 := Role1(instance)

	if err := controllerutil.SetControllerReference(instance, role1, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundrole1 := &rbacv1.Role{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: role1.Name, Namespace: role1.Namespace}, foundrole1)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new Role", "Role.Namespace", role1.Namespace, "Role.Name", role1.Name)
		err = r.client.Create(context.TODO(), role1)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		! cmp.Equal(foundrole1.Rules, role1.Rules) ) {
	reqLogger.Info("Role has changed", "Role.Namespace", role1.Namespace, "Role.Name", role1.Name)
	foundrole1.Rules = role1.Rules 
	err = r.client.Update(context.TODO(), foundrole1)
		if err != nil {
			reqLogger.Error(err, "Failed to update Role", "Role.Namespace", role1.Namespace, "Role.Name", role1.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

    rolebind1 := RoleBind1(instance)

	if err := controllerutil.SetControllerReference(instance, rolebind1, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundrolebind1 := &rbacv1.RoleBinding{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: rolebind1.Name, Namespace: rolebind1.Namespace}, foundrolebind1)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new RoleBinding", "RoleBinding.Namespace", rolebind1.Namespace, "RoleBinding.Name", rolebind1.Name)
		err = r.client.Create(context.TODO(), rolebind1)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}


    cron1 := Cron1(instance)

	if err := controllerutil.SetControllerReference(instance, cron1, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundcron1 :=  &batchv1beta1.CronJob{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: cron1.Name, Namespace: cron1.Namespace}, foundcron1)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new CronJob", "CronJob.Namespace", cron1.Namespace, "CronJob.Name", cron1.Name)
		err = r.client.Create(context.TODO(), cron1)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		foundcron1.Spec.Schedule != cron1.Spec.Schedule ||
		foundcron1.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image != cron1.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image ) {
	reqLogger.Info("CronJob1 has changed", "CronJob.Namespace", cron1.Namespace, "CronJob.Name", cron1.Name)
	foundcron1.Spec.Schedule = cron1.Spec.Schedule
	foundcron1.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image = cron1.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image 
	err = r.client.Update(context.TODO(), foundcron1)
		if err != nil {
			reqLogger.Error(err, "Failed to update CronJob", "CronJob.Namespace", cron1.Namespace, "CronJob.Name", cron1.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

	cron2 := Cron2(instance)

	if err := controllerutil.SetControllerReference(instance, cron2, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundcron2 :=  &batchv1beta1.CronJob{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: cron2.Name, Namespace: cron2.Namespace}, foundcron2)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new CronJob", "CronJob.Namespace", cron2.Namespace, "CronJob.Name", cron2.Name)
		err = r.client.Create(context.TODO(), cron2)
		if err != nil {
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required
		foundcron2.Spec.Schedule != cron2.Spec.Schedule ||
		foundcron2.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image != cron2.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image ) {
	reqLogger.Info("CronJob2 has changed", "CronJob.Namespace", cron2.Namespace, "CronJob.Name", cron2.Name)
	foundcron2.Spec.Schedule = cron2.Spec.Schedule
	foundcron2.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image = cron2.Spec.JobTemplate.Spec.Template.Spec.Containers[0].Image 
	err = r.client.Update(context.TODO(), foundcron2)
		if err != nil {
			reqLogger.Error(err, "Failed to update CronJob", "CronJob.Namespace", cron2.Namespace, "CronJob.Name", cron2.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}


	bckvol := &corev1.PersistentVolumeClaim{}

	if instance.Spec.Backupstorageclass != "" {
	bckvol = BackupVolDyn(instance)
	}
	if instance.Spec.Backuppvname != "" {
	bckvol = BackupVolPV(instance)
	}
	

	if err := controllerutil.SetControllerReference(instance, bckvol, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundbckvol := &corev1.PersistentVolumeClaim{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: bckvol.Name, Namespace: bckvol.Namespace}, foundbckvol)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new PersistentVolumeClaim", "PersistentVolumeClaim.Namespace", bckvol.Namespace, "PersistentVolumeClaim.Name", bckvol.Name)
		err = r.client.Create(context.TODO(), bckvol)
		if err != nil {
			return reconcile.Result{}, err
		}

		// Pod created successfully - don't requeue
		return reconcile.Result{}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

	}

    // sts 

	sts1 := Sts1(instance)

	if err := controllerutil.SetControllerReference(instance, sts1, r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	foundsts1 := &appsv1.StatefulSet{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: sts1.Name, Namespace: sts1.Namespace}, foundsts1)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Info("Creating a new StatefulSet", "StatefulSet.Namespace", sts1.Namespace, "StatefulSet.Name", sts1.Name)
		err = r.client.Create(context.TODO(), sts1)
		if err != nil {
			return reconcile.Result{}, err
		}

		// Pod created successfully - don't requeue
		return reconcile.Result{}, nil
	} else if (
		// conditions I am interested in - add to list if required	
	    *foundsts1.Spec.Replicas != *sts1.Spec.Replicas || 
		foundsts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceCPU] != sts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceCPU] || 
		foundsts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceMemory] != sts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceMemory] || 
		foundsts1.Spec.Template.Spec.Containers[0].Image != sts1.Spec.Template.Spec.Containers[0].Image ||
	    foundsts1.Spec.Template.Spec.Containers[0].Env[0].Value != strconv.Itoa(Restarts)) {
	reqLogger.Info("StatefulSet has changed", "From", foundsts1.Spec.Replicas, "To", sts1.Spec.Replicas)
		foundsts1.Spec.Replicas = sts1.Spec.Replicas
		foundsts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceCPU] = sts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceCPU]
		foundsts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceMemory] = sts1.Spec.Template.Spec.Containers[0].Resources.Limits[corev1.ResourceMemory]
		foundsts1.Spec.Template.Spec.Containers[0].Resources.Requests[corev1.ResourceCPU] = sts1.Spec.Template.Spec.Containers[0].Resources.Requests[corev1.ResourceCPU]
		foundsts1.Spec.Template.Spec.Containers[0].Resources.Requests[corev1.ResourceMemory] = sts1.Spec.Template.Spec.Containers[0].Resources.Requests[corev1.ResourceMemory]
		foundsts1.Spec.Template.Spec.Containers[0].Image = sts1.Spec.Template.Spec.Containers[0].Image
		foundsts1.Spec.Template.Spec.Containers[0].Env[0].Value = strconv.Itoa(Restarts)
	err = r.client.Update(context.TODO(), foundsts1)
		if err != nil {
			reqLogger.Error(err, "Failed to update StatefulSet", "StatefulSet.Namespace", sts1.Namespace, "StatefulSet.Name", sts1.Name)
			return reconcile.Result{}, err
		}
		// Item changed - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

    // end of sts
	// nothing we are interested in has changed - don't requeue
	return reconcile.Result{}, nil
}


// Service returns a service in the name/namespace as the cr
func Service1(cr *xtradbv1alpha1.XtraDB) *corev1.Service {
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name,
			Namespace: cr.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			ClusterIP: "None",
			Ports: []corev1.ServicePort{
				{
					Name:       "mysql",
					Port:       3306,
				},
				{
					Name:       "sst",
					Port:       4444,
				},
				{
					Name:       "replication",
					Port:       4567,
				},
				{
					Name:       "ist",
					Port:       4568,
				},
			},
			Selector: map[string]string{
				"release": cr.Name,
				"type": "cluster",
			},
		},
	}
}

// Service returns a service in the name/namespace as the cr
func Service2(cr *xtradbv1alpha1.XtraDB) *corev1.Service {
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name + "-cluster",
			Namespace: cr.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			Ports: []corev1.ServicePort{
				{
					Name:       "mysql",
					Port:       3306,
				},
				{
					Name:       "sst",
					Port:       4444,
				},
				{
					Name:       "replication",
					Port:       4567,
				},
				{
					Name:       "ist",
					Port:       4568,
				},
			},
			SessionAffinity: "ClientIP",
			Selector: map[string]string{
				"release": cr.Name,
				"type": "cluster",
			},
		},
	}
}


const (
   DefaultCores              = "500m"
   DefaultMemory             = "2Gi"
   DefaultImagePXC           = "jonhartley/k8s-pxc:latest"
   DefaultImageExporter      = "prom/mysqld-exporter:latest"
   DefaultImageMaint         = "jonhartley/k8s-pxc-maint:latest"
   DefaultImageFluentd       = "jonhartley/k8s-pxc-fluentd:latest"
   DefaultMainstoragesize    = "10G"
   DefaultMainstorageclass   = "standard"
   DefaultBackupEnabled      = false
   DefaultBackupstorageclass = "nfs"
   DefaultBackupstoragesize  = "40G"
   DefaultExtraConfig        = "max_allowed_packet=1G,innodb_buffer_pool_size=1G,max_connections=500,innodb_log_file_size=128M"
   DefaultWsrepConfig        = "wsrep_provider_options='gcache.size=512M;gcache.recover=yes;evs.send_window=512;evs.user_send_window=512'"
   DefualtBinlogConfig       = "expire_logs_days=1"
   DefaultNodeSelector       = true    
   DefaultNodeLabel          = "workload"
   DefaultNodeValue          = "persistence"
   DefaultTolerations        = true
   DefaultTolKey             = "workload"
   DefaultTolValue           = "persistence"
   DefaultRestarts           = 0 
)

func Sts1(cr *xtradbv1alpha1.XtraDB) *appsv1.StatefulSet {
	nodeselector := map[string]string{}
	tolerations := []corev1.Toleration{}
	if cr.Spec.NodeSelector == true {nodeselector = map[string]string{ cr.Spec.NodeLabel: cr.Spec.NodeValue }}
	if cr.Spec.Tolerations ==true {
  	if cr.Spec.TolKey == "" { cr.Spec.TolKey = DefaultTolKey }
	if cr.Spec.TolValue == "" { cr.Spec.TolValue = DefaultTolValue}
	tolerations = []corev1.Toleration{
		corev1.Toleration{
				Key: cr.Spec.TolKey,
				Operator: corev1.TolerationOpEqual,
				Value: cr.Spec.TolValue,
				Effect: corev1.TaintEffectNoSchedule,
			},
		}
	} //end of tolerations

	if cr.Spec.Cores == "" { cr.Spec.Cores = DefaultCores }
	if cr.Spec.Memory == "" { cr.Spec.Memory = DefaultMemory }
	if cr.Spec.ImagePXC == "" { cr.Spec.ImagePXC = DefaultImagePXC }
	if cr.Spec.ImageExporter == "" { cr.Spec.ImageExporter = DefaultImageExporter }
	if cr.Spec.ImageMaint == "" { cr.Spec.ImageMaint = DefaultImageMaint }
	if cr.Spec.ImageFluentd == "" { cr.Spec.ImageFluentd = DefaultImageFluentd }
	if cr.Spec.Mainstoragesize == "" { cr.Spec.Mainstoragesize = DefaultMainstoragesize }
	if cr.Spec.Mainstorageclass == "" { cr.Spec.Mainstorageclass = DefaultMainstorageclass }
	if cr.Spec.ExtraConfig == "" { cr.Spec.ExtraConfig = DefaultExtraConfig }
	if cr.Spec.BackupEnabled == false { cr.Spec.BackupEnabled = DefaultBackupEnabled }
	if Restarts == 0 { Restarts = DefaultRestarts }

	volmounts := []corev1.VolumeMount{{ Name: "datadir",MountPath: "/opt/mysql" }}
	initvolmounts := []corev1.VolumeMount{{ Name: "datadir",MountPath: "/mnt/data" }}
	initcmd := "chown -R 65535:65535 /mnt/data"
	backupdir := ""
	volumes := []corev1.Volume{}
	if cr.Spec.BackupEnabled == true {
		backupdir = "/mnt/backup"
		volumes = []corev1.Volume{ {
							Name: "backupvol",
							VolumeSource: corev1.VolumeSource{
										PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
										ClaimName: cr.Name +"-backupvol",
										},
									},
							},
						}
    	volmounts = []corev1.VolumeMount{{ Name: "datadir",MountPath: "/opt/mysql" },{ Name: "backupvol",MountPath: "/mnt/backup", SubPath: cr.Name + "-backup" }}
    	initvolmounts = []corev1.VolumeMount{{ Name: "datadir",MountPath: "/mnt/data" },{ Name: "backupvol",MountPath: "/mnt/backup", SubPath: cr.Name + "-backup" }}
    	initcmd = "chown -R 65535:65535 /mnt/data /mnt/backup"
	}

	containers := []corev1.Container{}

	conpxc := corev1.Container{
						Name:    "pxc",
						Image:   cr.Spec.ImagePXC,
						Ports: []corev1.ContainerPort{
						{ ContainerPort: 3306, Name: "mysql"},
						{ ContainerPort: 4444, Name: "sst" },
						{ ContainerPort: 4567, Name: "replication" },
						{ ContainerPort: 4568, Name: "ist" },
						},
						Resources: corev1.ResourceRequirements{ 
							Requests: map[corev1.ResourceName]resource.Quantity{ "cpu": resource.MustParse(cr.Spec.Cores), "memory": resource.MustParse(cr.Spec.Memory) }, 
						 	Limits: map[corev1.ResourceName]resource.Quantity{ "cpu": resource.MustParse(cr.Spec.Cores), "memory": resource.MustParse(cr.Spec.Memory) }, 
						 },
						Env: []corev1.EnvVar{
							{ Name: "ROLLINGRESTARTS", Value: strconv.Itoa(Restarts) },
							{
								Name: "MEM_LIMIT",
								ValueFrom: &corev1.EnvVarSource{
									ResourceFieldRef: &corev1.ResourceFieldSelector{ContainerName: "pxc",Resource: "limits.memory"},
								},
							},
							{
								Name: "CPU_LIMIT",
								ValueFrom: &corev1.EnvVarSource{
									ResourceFieldRef: &corev1.ResourceFieldSelector{ContainerName: "pxc",Resource: "limits.cpu"},
								},
							},
							{ Name: "CONFIG", Value: cr.Spec.ExtraConfig },
							{ Name: "WSREPCONFIG", Value: cr.Spec.WsrepConfig },
							{ Name: "BINLOGCONFIG", Value: cr.Spec.BinlogConfig },
							{ Name: "CLUSTER_NAME", Value: cr.Name },
							{ Name: "CLUSTER_JOIN", Value: cr.Name + "-cluster" },
							{ Name: "BACKUPDIR", Value: backupdir },
							{
								Name: "MYSQL_ROOT_PASSWORD",
								ValueFrom: &corev1.EnvVarSource{
									SecretKeyRef: &corev1.SecretKeySelector{
										LocalObjectReference: corev1.LocalObjectReference{
										Name: cr.Spec.SecretsName,
										},
										Key: "root",
										},
							},
							},
							{
								Name: "SSTUSER_PASSWORD",
								ValueFrom: &corev1.EnvVarSource{
									SecretKeyRef: &corev1.SecretKeySelector{
										LocalObjectReference: corev1.LocalObjectReference{
										Name: cr.Spec.SecretsName,
										},
										Key: "sstuser",
										},
							},
							},
							{
								Name: "MONITOR_PASSWORD",
								ValueFrom: &corev1.EnvVarSource{
									SecretKeyRef: &corev1.SecretKeySelector{
										LocalObjectReference: corev1.LocalObjectReference{
										Name: cr.Spec.SecretsName,
										},
										Key: "monitor",
										},
							},
							},
						},
						VolumeMounts: volmounts,
						ReadinessProbe: &corev1.Probe{
						Handler: corev1.Handler{
							Exec: &corev1.ExecAction{
								Command: []string {"/usr/bin/clustercheck.sh"},
								},
							},
							InitialDelaySeconds: 30, TimeoutSeconds: 5, PeriodSeconds: 10, FailureThreshold: 2,
					    },
						LivenessProbe: &corev1.Probe{
							Handler: corev1.Handler{
							Exec: &corev1.ExecAction{
								Command: []string {"/usr/bin/livecheck.sh"},
								},
							},
							InitialDelaySeconds: 10, TimeoutSeconds: 5, PeriodSeconds: 10, FailureThreshold: 3,
						},
						}

	conexporter := corev1.Container{
						Name:    "exporter",
						Image:   "prom/mysqld-exporter",
						Ports: []corev1.ContainerPort{{
							ContainerPort: 9104,
							Name:          "prometheus",
						}},
						Args: []string {
							"--collect.global_variables", 
							"--collect.engine_innodb_status", 
							"--collect.info_schema.innodb_cmp",
							"--collect.info_schema.innodb_metrics",
							"--collect.info_schema.innodb_cmpmem",
							"--collect.info_schema.tables.databases=*",
							"--collect.info_schema.innodb_tablespaces",
							"--collect.info_schema.processlist",
							"--collect.slave_hosts",
						},
						Env: []corev1.EnvVar{
							{
								Name: "DATA_SOURCE_NAME",
								ValueFrom: &corev1.EnvVarSource{
									SecretKeyRef: &corev1.SecretKeySelector{
										LocalObjectReference: corev1.LocalObjectReference{
										Name: cr.Spec.SecretsName,
										},
										Key: "exporterdsn",
										},
								},
							},
						},
						Resources: corev1.ResourceRequirements{ 
						 	Requests: map[corev1.ResourceName]resource.Quantity{ "cpu": resource.MustParse("20m"),"memory": resource.MustParse("80Mi") }, 
						 	Limits: map[corev1.ResourceName]resource.Quantity{ "cpu":    resource.MustParse("20m"),"memory": resource.MustParse("80Mi") }, 
						},
						}
	containers = append(containers,conpxc)
	containers = append(containers,conexporter)

	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}

	podAnnotations := map[string]string{
		"prometheus.io/port":   "9104",
    	"prometheus.io/scrape": "true",
    }

	dep := &appsv1.StatefulSet{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "apps/v1",
			Kind:       "StatefulSet",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Name,
			Namespace: cr.Namespace,
			Labels: labels,
		},
		Spec: appsv1.StatefulSetSpec{
			Replicas: &cr.Spec.Size,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			ServiceName: cr.Name,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
					Annotations: podAnnotations,
				},
				Spec: corev1.PodSpec{
					Affinity:  &corev1.Affinity{
					PodAntiAffinity: &corev1.PodAntiAffinity{
						RequiredDuringSchedulingIgnoredDuringExecution: []corev1.PodAffinityTerm{
							{
								LabelSelector: &metav1.LabelSelector{
									MatchExpressions: []metav1.LabelSelectorRequirement{
										{
											Key:      "app.kubernetes.io/instance",
											Operator: "In",
											Values:   []string{cr.Name},
										},
									},
								},
								TopologyKey: "kubernetes.io/hostname",
							},
						},
					},
					},
					NodeSelector: nodeselector,
					Tolerations: tolerations,
					InitContainers: []corev1.Container{
						{
						Name: "volume-mount",
						Image: "busybox",
						Command: []string {
							"sh", 
							"-c", 
							initcmd, 
						},
						VolumeMounts: initvolmounts,
						},
					},
					Containers: containers,
					Volumes: volumes,
				},
				},
			VolumeClaimTemplates: []corev1.PersistentVolumeClaim{
					    {
						ObjectMeta: metav1.ObjectMeta{
							Name: "datadir",
							//Annotations: VolAnnotations,
						},
						Spec: corev1.PersistentVolumeClaimSpec{
								AccessModes: []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
								Resources: corev1.ResourceRequirements{
									Requests: corev1.ResourceList{
										corev1.ResourceStorage: resource.MustParse("1Gi"),
			                          },
		                          },
		                        StorageClassName: &cr.Spec.Mainstorageclass,
								},
						},
			},
		},
	}
	return dep
}

func Pdb1(cr *xtradbv1alpha1.XtraDB) *policyv1beta1.PodDisruptionBudget {
	maxUnavailable := intstr.FromInt(1)
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &policyv1beta1.PodDisruptionBudget{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-pdb",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
		Spec: policyv1beta1.PodDisruptionBudgetSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			MaxUnavailable: &maxUnavailable,
		},
	}
}



func SA1(cr *xtradbv1alpha1.XtraDB) *corev1.ServiceAccount {
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &corev1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-backup",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
	}
}

func Role1(cr *xtradbv1alpha1.XtraDB) *rbacv1.Role{
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &rbacv1.Role{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-backup",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
		Rules: []rbacv1.PolicyRule{
			{
			APIGroups: []string{ "" },
			Resources: []string{ "pods", "pods/exec", "persistentvolumeclaims", "secrets" },
			Verbs: []string{ "get", "list", "create", "update", "patch", "delete" },
            ResourceNames: []string{ cr.Name, cr.Name + "-0", cr.Name + "-1", cr.Name + "-2", "datadir-" + cr.Name + "-0", "datadir-" + cr.Name + "-1", "datadir-" + cr.Name + "-2" },
        	},
			{
			APIGroups: []string{ "apps"},
			Resources: []string{ "statefulsets", "statefulsets/scale" },
			Verbs: []string{ "get", "list", "update", "patch" },
            ResourceNames: []string{ cr.Name },
        	},
			{
			APIGroups: []string{ "batch" },
			Resources: []string{ "cronjobs", "jobs" },
			Verbs: []string{ "get", "list", "create", "update", "patch" },
            ResourceNames: []string{ cr.Name, cr.Name + "-backup" },
        	},
		},
	}
}

func RoleBind1(cr *xtradbv1alpha1.XtraDB) *rbacv1.RoleBinding{
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &rbacv1.RoleBinding{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-backup",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
		RoleRef: rbacv1.RoleRef{
  			APIGroup: "rbac.authorization.k8s.io",
  			Kind: "Role",
  			Name: cr.Name + "-backup",
		},
		Subjects: []rbacv1.Subject{
			{
			Kind: "ServiceAccount",
			Name:  cr.Name + "-backup",
			},
        },
	}
}

func Cron1(cr *xtradbv1alpha1.XtraDB) *batchv1beta1.CronJob {
	FailedHist := int32(1)
	SuccessHist := int32(1)
    Suspended := bool(false)
    Backoff := int32(0)
    if cr.Spec.ImageMaint == "" { cr.Spec.ImageMaint = DefaultImageMaint }
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &batchv1beta1.CronJob{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-backup",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
		Spec: batchv1beta1.CronJobSpec{
   			Schedule: "5 */4 * * *",
            ConcurrencyPolicy: "Forbid",
  			FailedJobsHistoryLimit: &FailedHist,
  			SuccessfulJobsHistoryLimit: &SuccessHist,
  			Suspend: &Suspended,
  			JobTemplate: batchv1beta1.JobTemplateSpec{
  				Spec: jobv1.JobSpec{
  					BackoffLimit: &Backoff,
  					Template: corev1.PodTemplateSpec{
  						Spec: corev1.PodSpec{
  							ServiceAccountName: cr.Name + "-backup",
  							RestartPolicy: "Never",
  							Containers: []corev1.Container{
								{
								Name:    "maint",
								Image:   cr.Spec.ImageMaint,
								Args: []string {
								"-c", 
								"/bin/kubectl exec -i -c pxc "+ cr.Name +"-0  /opt/mysql/bin/xtrabackup.sh", 
								},
								},
  							},
  						},
  					},
  				},
  			},
		},
	}
}

func Cron2(cr *xtradbv1alpha1.XtraDB) *batchv1beta1.CronJob {
	FailedHist := int32(1)
	SuccessHist := int32(1)
    Suspended := bool(false)
    Backoff := int32(0)
    if cr.Spec.ImageMaint == "" { cr.Spec.ImageMaint = DefaultImageMaint }
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
	return &batchv1beta1.CronJob{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-maint",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
		Spec: batchv1beta1.CronJobSpec{
   			Schedule: "0 * * * *",
            ConcurrencyPolicy: "Forbid",
  			FailedJobsHistoryLimit: &FailedHist,
  			SuccessfulJobsHistoryLimit: &SuccessHist,
  			Suspend: &Suspended,
  			JobTemplate: batchv1beta1.JobTemplateSpec{
  				Spec: jobv1.JobSpec{
  					BackoffLimit: &Backoff,
  					Template: corev1.PodTemplateSpec{
  						Spec: corev1.PodSpec{
  							ServiceAccountName: cr.Name + "-backup",
  							RestartPolicy: "Never",
  							Containers: []corev1.Container{
								{
								Name:    "maint",
								Image:   cr.Spec.ImageMaint,
								Args: []string {
								"-c", 
								"/scripts/logrotate.sh", 
								},
								Env: []corev1.EnvVar{
									{ Name: "CLUSTER", Value: cr.Name },
									},
								},
  							},
  						},
  					},
  				},
  			},
		},
	}
}

func BackupVolDyn(cr *xtradbv1alpha1.XtraDB) *corev1.PersistentVolumeClaim {
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
 	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-backupvol",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
 		Spec: corev1.PersistentVolumeClaimSpec{
 								AccessModes: []corev1.PersistentVolumeAccessMode{corev1.ReadWriteMany},
 								Resources: corev1.ResourceRequirements{
 									Requests: corev1.ResourceList{
										corev1.ResourceStorage: resource.MustParse(cr.Spec.Backupstoragesize),
 			                        },
 			                    },
			                    StorageClassName: &cr.Spec.Backupstorageclass,
 								},
 	}
}

func DataDir(cr *xtradbv1alpha1.XtraDB, nodenum int32) *corev1.PersistentVolumeClaim {
	if cr.Spec.Mainstoragesize == "" { cr.Spec.Mainstoragesize = DefaultMainstoragesize }
	if cr.Spec.Mainstorageclass == "" { cr.Spec.Mainstorageclass = DefaultMainstorageclass }

	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
 	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:         "datadir-" + cr.Name + "-" + strconv.Itoa(int(nodenum)),
			Namespace:    cr.Namespace,
			Labels: labels,
		},
 		Spec: corev1.PersistentVolumeClaimSpec{
 								AccessModes: []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
 								Resources: corev1.ResourceRequirements{
 									Requests: corev1.ResourceList{
										corev1.ResourceStorage: resource.MustParse(cr.Spec.Mainstoragesize),
 			                        },
 			                    },
			                    StorageClassName: &cr.Spec.Mainstorageclass,
 								},
 	}
}


func BackupVolPV(cr *xtradbv1alpha1.XtraDB) *corev1.PersistentVolumeClaim {
	emptyClass := ""
	labels := map[string]string{
		"release": cr.Name,
		"type": "cluster",
	}
 	return &corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:         cr.Name + "-backupvol",
			Namespace:    cr.Namespace,
			Labels: labels,
		},
 		Spec: corev1.PersistentVolumeClaimSpec{
 								AccessModes: []corev1.PersistentVolumeAccessMode{corev1.ReadWriteMany},
 								Resources: corev1.ResourceRequirements{
 									Requests: corev1.ResourceList{
										corev1.ResourceStorage: resource.MustParse(cr.Spec.Backupstoragesize),
 			                        },
 			                    },
			                    VolumeName: cr.Spec.Backuppvname,
			                    StorageClassName: &emptyClass,
 								},
 	}
}