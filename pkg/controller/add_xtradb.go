package controller

import (
	"home.org/xtradb-operator/pkg/controller/xtradb"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, xtradb.Add)
}
